<?php

namespace App\Http\Controllers;

use App\Services\EmailService;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;

class ApplicationController extends Controller
{
    public function __construct(
        private readonly EmailService $emailService
    ) {
    }

    public function create(Request $request)
    {
        $validated = $request->validate([
            'applicant_name' => 'required|string|max:255',
            'date_of_birth' => 'required|date_format:m/d/Y',
            'social_security_number' => 'required|digits:9',
            'street_address' => 'sometimes|string|max:255',
            'city' => 'sometimes|string|max:255',
            'state' => 'sometimes|string|max:20',
            'zip_code' => 'sometimes|string|max:10',
            'package_id' => 'required|int|max:3',
            'alias_name' => 'sometimes|string|max:255',
            'gender' => 'sometimes|in:Male,Female,Other',
        ]);

        $this->emailService->send($validated);
    }
}
