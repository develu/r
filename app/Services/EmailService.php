<?php

namespace App\Services;

use Illuminate\Support\Facades\Log;

class EmailService
{
    public function send(array $data): void
    {
        Log::info('Email sent', $data);
    }
}
